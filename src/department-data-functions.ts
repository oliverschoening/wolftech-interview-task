import IRawDataItem from "./interfaces/IRawDataItem";
import DataNode from "./classes/DataNode";

export function createDataNodeTree ( flatData: IRawDataItem[] ) {

    const nodes = flatData.map( raw => new DataNode( raw ) );

    const tree = nodes
        .map( parent => {
            parent.___Children = nodes.filter( child => child.DepartmentParent_OID === parent.OID );
            parent.___Children.forEach( child => child.___Parent = parent );
            return parent;
        } )
        .filter( item => item.DepartmentParent_OID === null );

    return tree;
}

/**
 * goes through the node tree and sets nodes with matching @oids to @value
 * ignores nodes that have #Children.length > 0
 */
export function setIsSelectedByOID ( arr: DataNode[], isSelectedOIDs: number[] ) {
    return setPropByArrayValueToValue( arr, isSelectedOIDs, "OID", "___IsSelected", true );
}

/*
export function setIsExpandedByOID ( arr: DataNode[], isExpandedOIDs: number[] ) {
    return setPropByArrayValueToValue( arr, isExpandedOIDs, "OID", "___IsExpanded", true );
}
*/

function setPropByArrayValueToValue ( arr: DataNode[], compareValues: number[], compareKey: string, setKey: string, value = true ) {

    arr.forEach( node => {
        if ( node.___Children.length === 0 ) {
            compareValues.forEach( cv => {
                if ( node[ compareKey ] === cv ) {
                    node[ setKey ] = value;
                }
            } );
        } else {
            setPropByArrayValueToValue( node.___Children, compareValues, compareKey, setKey, value );
        }
    } );

    return arr;

}

export function setIsExpandedByIsSelected ( arr: DataNode[] ) {


    const flatNodeArray: DataNode[] = [];
    function getAllNodes ( nodes: DataNode[] ) {
        nodes.forEach( node => {
            flatNodeArray.push( node );
            if ( node.___Children.length > 0 ) {
                getAllNodes( node.___Children );
            }
        } );
    }

    getAllNodes( arr );

    flatNodeArray.forEach( node => {
        if ( node.___IsSelected ) {
            node.___IsExpanded = true;
        }
    } );

    function recursiveExpand ( node: DataNode ) {
        if ( node.___IsExpanded && node.___Parent ) {
            node.___Parent.___IsExpanded = true;
            recursiveExpand( node.___Parent );
        }
    }

    flatNodeArray.forEach( recursiveExpand );

    return arr;
}


/**
 * goes through the node tree and returns an array of all the OIDs equal to value
 */
export function getIsSelectedOIDs ( arr: DataNode[], value = true ) {

    const oids: number[] = [];

    function recur ( _arr: DataNode[], _value = true ) {
        _arr.forEach( node => {
            if ( node.___IsSelected === _value ) {
                oids.push( node.OID );
            }

            if ( node.___Children.length > 0 ) {
                recur( node.___Children, _value );
            }
        } );
    }

    recur( arr, value );

    return oids;

}
