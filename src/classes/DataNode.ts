import IRawDataItem from "../interfaces/IRawDataItem";
import IDataNode from "../interfaces/IDataNode";

export default class DataNode implements IDataNode {
    OID: number;
    Title: string;
    Color: string;
    DepartmentParent_OID: null | number;

    ___IsSelected: boolean;
    ___Children: DataNode[];
    ___Parent: DataNode | null;
    ___IsExpanded: boolean;

    constructor ( raw: IRawDataItem ) {
        this.OID = raw.OID;
        this.Title = raw.Title;
        this.Color = raw.Color;
        this.DepartmentParent_OID = raw.DepartmentParent_OID;

        this.___IsSelected = false;
        this.___Children = [];
        this.___Parent = null;
        this.___IsExpanded = false;
    }

    get ___AmountOfNestedChildrenSelected (): number {
        let amount = 0;

        function repeat ( arr: DataNode[] ) {

            arr.forEach( child => {
                if ( child.___IsSelected && child.___Children.length === 0 ) {
                    amount++;
                }
                if ( child.___Children.length > 0 ) {
                    repeat( child.___Children );
                }
            } );

        }

        repeat( this.___Children );

        return amount;
    }

    get ___AmountOfNestedChildren () {
        let amount = 0;

        function repeat ( arr: DataNode[] ) {

            arr.forEach( child => {
                if ( child.___Children.length === 0 ) {
                    amount++;
                }
                if ( child.___Children.length > 0 ) {
                    repeat( child.___Children );
                }
            } );

        }

        repeat( this.___Children );

        return amount;
    }

    get ___AmountOfNestedParentsAndChildren () {
        let amount = 0;
        function repeat ( arr: DataNode[] ) {

            arr.forEach( child => {
                amount++;
                if ( child.___Children.length > 0 ) {
                    repeat( child.___Children );
                }
            } );

        }

        repeat( this.___Children );

        return amount;
    }

    get ___HasAnyNestedSelectedChildren () {

        let hasAny = false;

        function repeat ( arr: DataNode[] ) {
            if ( hasAny === true ) {
                return;
            }

            arr.forEach( child => {
                if ( child.___IsSelected ) {
                    hasAny = true;
                }
                if ( child.___Children.length > 0 ) {
                    repeat( child.___Children );
                }
            } );

        }

        repeat( this.___Children );

        return hasAny;
    }
}
