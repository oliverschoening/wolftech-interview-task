import IRawDataItem from "./IRawDataItem";

// prefixed with '___' to avoid name collisions with unknown production data
export default interface IDataNode extends IRawDataItem {
    ___IsSelected: boolean;
    ___AmountOfNestedChildren: number;
    ___AmountOfNestedChildrenSelected: number;
    ___Children: IDataNode[];
    ___Parent: IDataNode | null;
    ___HasAnyNestedSelectedChildren: boolean;
    ___IsExpanded: boolean;
    ___AmountOfNestedParentsAndChildren: number;
}
