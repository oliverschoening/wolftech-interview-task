export default interface IRawDataItem {
    OID: number;
    Title: string;
    Color: string;
    DepartmentParent_OID: null | number;
}
