export default interface IDataServiceError {
    id: number;
    message: string;
}
