import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { HttpClientModule } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { MatButtonModule, MatCheckboxModule, MatDialogModule } from "@angular/material";

import { AppComponent } from "./app.component";
import { DataTreeComponent } from "./data-tree/data-tree.component";

@NgModule( {
  declarations: [
    AppComponent,
    DataTreeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatDialogModule
  ],
  providers: [],
  bootstrap: [ AppComponent ]
} )
export class AppModule { }
