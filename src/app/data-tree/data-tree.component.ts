import { Component, OnInit, Input } from "@angular/core";

import DataNode from "../../classes/DataNode";

@Component( {
  selector: "app-data-tree",
  templateUrl: "./data-tree.component.html",
  styleUrls: [ "./data-tree.component.css" ]
} )
export class DataTreeComponent implements OnInit {
  checkboxDataNodeSelected: boolean;

  @Input() nodes: DataNode[];
  @Input() preExpand: boolean;
  @Input() nestingDept = 0;

  ngOnInit () {
    console.log( "init" );
    console.log( this.nodes );
  }

  onIsSelectedCheckboxChange ( event: any, oid: number ) {
    const found = this.nodes.find( node => node.OID === oid );
    if ( found ) {
      found.___IsSelected = event.checked;
    }
  }

  toggleNodeExpansion ( node: DataNode ) {
    node.___IsExpanded = !node.___IsExpanded;
  }
}
