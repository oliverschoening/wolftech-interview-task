import { Component, OnInit } from "@angular/core";

import { DepartmentDataService } from "./department-data.service";
import DataNode from "../classes/DataNode";
import IDepartmentServiceError from "../interfaces/IDepartmentServiceError";
import DATA from "../DATA";
import { getIsSelectedOIDs } from "../department-data-functions";

@Component( {
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: [ "./app.component.css" ]
} )
export class AppComponent implements OnInit {
  title = "Wolftech Interview Task";

  dataNodePreSelectedOIDs = "";
  dataNodeURLOrJSON = "";
  preExpand = true;
  useSrcData = false;

  selectedOIDs = "";
  showSelectedOIDsModal = false;

  dataNodeTree: DataNode[] = [];
  errorMessages: IDepartmentServiceError[] = [];

  constructor ( private departmentDataService: DepartmentDataService ) { }

  ngOnInit () {
    /*
    this.departmentDataService.getErrorMessages().subscribe( errorMessages => {
      this.errorMessages = errorMessages;
    } );
    */
    // this.getData();
    this.errorMessages = this.departmentDataService.errorMessages;
  }

  getPreSelectedOIDs (): number[] {
    return this.dataNodePreSelectedOIDs
      .split( "," )
      .map( n => n.trim() )
      .map( n => parseInt( n, undefined ) );
  }

  getData (): void {
    this.getDataNodeTree();
  }

  getDataNodeTree (): void {

    let data = this.dataNodeURLOrJSON;
    if ( this.useSrcData ) {
      data = JSON.stringify( DATA );
    }

    this.departmentDataService.getDepartmentsAsNodeTree( data, this.getPreSelectedOIDs(), this.preExpand )
      .subscribe( dataNodeTree => this.dataNodeTree = dataNodeTree );
  }

  removeErrorMessage ( id: number ) {
    this.departmentDataService.removeErrorMessage( id );
  }

  onShowSelectedOIDsButtonClicked () {

    console.log( getIsSelectedOIDs( this.dataNodeTree ) );
    this.selectedOIDs = getIsSelectedOIDs( this.dataNodeTree ).join( ", " );

    if ( this.selectedOIDs.trim() ) {
      this.showSelectedOIDsModal = true;
    }
  }


}
