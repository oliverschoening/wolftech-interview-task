import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { catchError, map, tap } from "rxjs/operators";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import DataNode from "../classes/DataNode";
import IRawDataItem from "../interfaces/IRawDataItem";
import IDepartmentServiceError from "../interfaces/IDepartmentServiceError";
import { createDataNodeTree, setIsSelectedByOID, setIsExpandedByIsSelected } from "../department-data-functions";

const httpOptions = {
  headers: new HttpHeaders( { "Content-Type": "application/json" } )
};

@Injectable( {
  providedIn: "root"
} )
export class DepartmentDataService {

  errorMessages: IDepartmentServiceError[] = [];
  private errorMessageIdCount = 0;

  constructor ( private http: HttpClient ) { }

  getDepartmentsAsNodeTree ( inputData: string, selectedOIDs: number[] = [], preExpand = true ): Observable<DataNode[]> {

    function generateDataNodes ( data: string | IRawDataItem[] ) {
      const tree = createDataNodeTree( JSON.parse( inputData ) as IRawDataItem[] );
      if ( preExpand ) {
        return setIsExpandedByIsSelected( setIsSelectedByOID( tree, selectedOIDs ) );
      } else {
        return setIsSelectedByOID( tree, selectedOIDs );
      }
    }

    // either JSON or URL
    if ( this.isJSON( inputData ) ) {

      return of( generateDataNodes( inputData ) );

    } else {
      console.log( "fetch by url..." );
      return this.http.get<IRawDataItem[]>( inputData )
        .pipe(
          map( data => {
            const tree = createDataNodeTree( data );
            if ( preExpand ) {
              return setIsExpandedByIsSelected( setIsSelectedByOID( tree, selectedOIDs ) );
            } else {
              return setIsSelectedByOID( tree, selectedOIDs );
            }
          } ),
          catchError( this.handleError<DataNode[]>( `
            Please make sure that you are using a working URL or
            that your JSON object is properly formatted.
          `, [] ) )
        );
    }

  }

  getErrorMessages () {
    return of( this.errorMessages );
  }

  removeErrorMessage ( id: number ) {
    const found = this.errorMessages.find( message => message.id === id );
    if ( found ) {
      const index = this.errorMessages.indexOf( found );
      if ( index > -1 ) {
        this.errorMessages.splice( index, 1 );
      }
    }
  }

  private addErrorMessage ( message: string ) {
    this.errorMessages.length = 0;
    this.errorMessages.push( { id: this.errorMessageIdCount++, message } );
  }

  private isJSON ( input: string ): boolean {
    let tmp = true;
    try {
      JSON.parse( input );
    } catch ( e ) {
      console.log( e );
      tmp = false;
    }
    return tmp;
  }

  /**
 * Handle Http operation that failed.
 * Let the app continue.
 * @param operation - name of the operation that failed
 * @param result - optional value to return as the observable result
 */
  private handleError<T> ( operation = "operation", result?: T ) {
    return ( error: any ): Observable<T> => {

      this.addErrorMessage( operation );
      // Let the app keep running by returning an empty result.
      return of( result as T );
    };
  }
}
