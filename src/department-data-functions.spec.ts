import { createDataNodeTree, setIsSelectedByOID } from "./department-data-functions";

describe( "DepartmentDataFunctions", () => {

    const data_1 = [
        {
            "OID": 1,
            "Title": "a",
            "Color": "#EB5F25",
            "DepartmentParent_OID": null,
        }, {
            "OID": 2,
            "Title": "b",
            "Color": "#EB5F25",
            "DepartmentParent_OID": 1,
        },
        {
            "OID": 3,
            "Title": "c",
            "Color": "#EB5F25",
            "DepartmentParent_OID": null,
        }
    ];

    const data_2 = [
        {
            "OID": 1,
            "Title": "a",
            "Color": "#EB5F25",
            "DepartmentParent_OID": null,
        }, {
            "OID": 2,
            "Title": "b",
            "Color": "#EB5F25",
            "DepartmentParent_OID": 1,
        },
        {
            "OID": 3,
            "Title": "c",
            "Color": "#EB5F25",
            "DepartmentParent_OID": 2,
        }
    ];

    it( "should create a nested array with 2 root items and 1 child inside the first", () => {
        const tree = createDataNodeTree( data_1 );

        expect( tree.length ).toEqual( 2 );
        expect( tree[ 0 ].___Children.length ).toEqual( 1 );
    } );

    it( "should create a nested array of 3 items tested inside each other", () => {
        const tree = createDataNodeTree( data_2 );

        expect( tree.length ).toEqual( 1 );
        expect( tree[ 0 ].___Children.length ).toEqual( 1 );
        expect( tree[ 0 ].___Children[ 0 ].___Children.length ).toEqual( 1 );
    } );
} );

